# gw-comp

This package (really just a set of scripts) is facilitates visual and numerical
comparisons between template- and wavelet-based reconstructions of BBH signals.

This is really just my test clone

## Installation / Setup
Setup requires a modern version of virtualenv.  Follow [these
instructions](https://ldas-jobs.ligo.caltech.edu/~cbc/docs/pycbc/install_virtualenv.html)
before proceeding.

Installation instructions assume:
 * LALSuite (`lalinference_o2` branch) has been installed to
   `${HOME}/opt/lscsoft/lalinference_o2` (see [Installing LALSuite](#installing-lalsuite))
 * Repository has been cloned to `${HOME}/src/lscsoft/gw-compare`
 * Your virtual environments live in `${HOME}/virtualenvs`

 Clone this repository: 

 ```
 git clone git@git.ligo.org:james-clark/gw-compare.git ${HOME}/src/lscsoft/gw-compare
 ```
 
 Run:

 ```
 sh ${HOME}/src/lscsoft/gw-compare/create-virtualenv.sh
 ```

 Activate your virtual environment with (will already be active following
    installation):

 ```
 source ${HOME}/virtualenvs/gw-compare/bin/activate
 ```


### Installing LALSuite

Copies of the `lalinference_o2` branch are widely available but if you want to
install it yourself:
 Clone:
 
 ```
 git clone git@git.ligo.org:lscsoft/lalsuite.git ${HOME}/src/lscsoft/lalinference_o2
 ```

 Switch to a local branch of `lalinference_o2`
 
 ```
 cd ${HOME}/src/lscsoft/lalinference_o2
 git checkout -b lalinference_o2 origin/lalinference_o2

 ```

 Configure and install:

 ```
 ./00boot && ./configure --prefix=$HOME/opt/lscsoft/lalinference_o2 --enable-swig-python && make -j install
 ```
 

### Full list of dependencies:

* numpy, scipy
* [gwpy](https://gwpy.github.io/)
* [pycbc](http://ligo-cbc.github.io/pycbc/latest/html/install.html)
* [gw-reconstruct](https://git.ligo.org/bfarr/gw-reconstruct)
* LALSuite: lalinference_o2 branch

## Uses

The following operations are currently supported:
 * [Q-scans](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#making-q-scans)
 * [Time-domain waveform reconstruction plots & overlaps](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#comparing-reconstructions)
 * [Residuals
   Analysis](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#residuals):
  * [Construct frame files with h(t) minus MAP waveform, given posterior samples](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#making-residuals-frames)
  * [Fitting-factor
    calculation](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#residuals)

# Making Q-Scans
 * [gw-comp/gw-comp/gwcomp_qscans.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_qscans.py)

```
usage: gwcomp_qscans.py [-h] [--channel CHANNEL] [--trigtime TRIGTIME]
                        [--datalen DATALEN] [--plotwin PLOTWIN]
                        [--f-min F_MIN] [--f-max F_MAX]
                        [--searchwin SEARCHWIN] [--linearfreqs]
                        [--H1-cache H1_CACHE] [--L1-cache L1_CACHE]
                        [--output-path OUTPUT_PATH] [--snr-max SNR_MAX]
                        [--xlims XLIMS]

Construct publication-quality Q-scans. Use of gwpy allows us to read data
directly using NDS2. Also supports frame cache files for local frames. Note
axes for plots are limited to values appropriate for GW170104.

optional arguments:
  -h, --help            show this help message and exit
  --channel CHANNEL
  --trigtime TRIGTIME
  --datalen DATALEN
  --plotwin PLOTWIN
  --f-min F_MIN
  --f-max F_MAX
  --searchwin SEARCHWIN
  --linearfreqs
  --H1-cache H1_CACHE
  --L1-cache L1_CACHE
  --output-path OUTPUT_PATH
  --snr-max SNR_MAX
  --xlims XLIMS         x-axis limits for main panel
  ```
  
## Example: GW150914

```
python gw-comp/gw-comp/gwcomp_qscans.py \
  --channel DCS-CALIB_STRAIN_C02 \
  --datalen 4 --plotwin 0.5 --searchwin 0.5 \
  --f-min 14 --f-max 600 --snr-max 10  \
  --xlims "0.25 0.45"
  --trigtime 1126259462.42
  ```

![](https://git.ligo.org/james-clark/gw-comp/raw/master/examples/H1L1-DCS-CALIB_STRAIN_C02-GW150914-qscan.png)
  

# Comparing Reconstructions
The workflow is a little more involved with 3 post-processing tasks:
1. [Reconstruct & Whiten Template Waveform](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#reconstructing-whitened-lal-approximants-from-posterior-samples-and-bayeswave-psds)
2. [Compute overlaps & other figures of
   merit](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#computing-overlaps)
3. [Produce plots and summary
   statistics](https://git.ligo.org/james-clark/gw-comp/blob/master/README.md#plotting-printing-the-results)

## Input Data
 * Posterior samples from LALInference (e.g., posterior_samples.dat)
 * BayesWave PSDs (e.g., IFO0_psd.dat)
 * BayesWave whitened instrument responses (e.g., signal_recovered_whitened_waveform.dat.0)
 * CWB whitened responses (e.g., H1_gw170104_pe_cwb_CO1.dat) [optional]

## Reconstructing Whitened LAL Approximants From Posterior Samples and BayesWave PSDs
 * [gw-comp/gw-comp/gwcomp_reclal.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_reclal.py)

 1. Loads posterior samples
 2. Generates waveforms for Nwaves samples
 3. Whitens using BayesWave PSD
 4. Saves to text file


```
Usage: gwcomp_reclal.py [options]

Reconstructs LALInference waveforms.  Make waveforms using samples drawn from
the posterior distribution on BBH approximant parameters from LALInference.
This seems to work a lot better with posterior samples from lalinference_nest.

Options:
  -h, --help            show this help message and exit
  --bw-dir=BW_DIR       
  --li-samples-file=LI_SAMPLES_FILE
  --srate=SRATE         
  --li-epoch=LI_EPOCH   
  --trigtime=TRIGTIME   
  --nwaves=NWAVES       
  --approx=APPROX       
  --duration=DURATION   
  --make-plots 
  ```

### Example: GW150914

In the example below, GW150914 is BayesWave PSD directory.   We assume the PSDs
keep their original naming scheme from BayesWave (IFO0_psd.dat etc).


```
gwcomp_reclal.py --bw-dir GW150914 \
    --li-samples-file GW150914/posterior_samples.dat
    --li-epoch 1126259460.42 --trigtime 1126259462.42 --srate 1024 \
     --make-plots 
```

Produces:
 * waveforms.npy: array of draws from waveform posterior, equivalent to BayesWave output
 * H1_summary_waveforms.txt: summary-statistics waveforms (Max-L, MAP, 90% CIs)
 * Time-domain plots (optional)

Using the lalinference_o2 branch of LALSuite, IMRPhenomPv2 and recent (Feb 2017)
priors:
<img width=250px src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/H1-GW150914-white_LI_reconstruction.png"><img width=250px  src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/L1-GW150914-white_LI_reconstruction.png">

## Computing Overlaps
 * [gw-comp/gw-comp/gwcomp_driver.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_driver.py) 

```
usage: gwcomp_driver.py [-h] [--li-waveforms LI_WAVEFORMS]
                        [--cwb-waveforms CWB_WAVEFORMS]
                        [--li-summary-waveforms LI_SUMMARY_WAVEFORMS]
                        [--injections] [--cwb-injections] --bw-dir BW_DIR
                        [--srate SRATE] [--title TITLE] [--bw-epoch BW_EPOCH]
                        [--li-epoch LI_EPOCH] [--trigtime TRIGTIME]
                        [--nwaves NWAVES] [--flow FLOW] --output-filename
                        OUTPUT_FILENAME [--save-for-publication]
                        [--nr-dir NR_DIR] [--duration DURATION]

Perform the main processing for waveform reconstruction comparisons. This
script computes overlaps and residuals between burst reconstructions and
LALInference reconstructions. LALInference reconstructions are obtained by
loading ascii files produced using gwcomp_reclal.py. Optional data products
include a pickle containing the main summary statistics and data required for
publication figures and results, produced using gwcomp_writepaper.py

optional arguments:
  -h, --help            show this help message and exit
  --li-waveforms LI_WAVEFORMS
  --cwb-waveforms CWB_WAVEFORMS
  --li-summary-waveforms LI_SUMMARY_WAVEFORMS
  --injections
  --cwb-injections
  --bw-dir BW_DIR
  --srate SRATE
  --title TITLE
  --bw-epoch BW_EPOCH
  --li-epoch LI_EPOCH
  --trigtime TRIGTIME
  --nwaves NWAVES
  --flow FLOW
  --output-filename OUTPUT_FILENAME
  --save-for-publication
  --nr-dir NR_DIR
  --duration DURATION
```

### Example: GW150914
This example assumes we have BayesWave results (PSDs and waveform posterior
draws) and the reconstructed LALInference waveforms (see preceeding section) in
a directory called GW150914
```
gwcomp_driver.py --bw-dir GW150914 --li-waveforms GW150914/waveforms.npy \
    --trigtime 1126259462.42 --li-epoch 1126259460.42 --bw-epoch 1126259460.42 \
    --li-summary-waveforms \
    GW150914/H1_summary_waveforms.txt GW150914/H1_summary_waveforms.txt \
    --flow 32 --srate 1024 \
    --save-for-publication 
```

This produces a python pickle which contains a dictionary of all waveforms,
overlaps samples and corresponding point-estimates.  We also dump out text files
with point-estimates of the BayesWave reconstructed SNR and overlap between the
reconstructions which can be useful for characterization:
 * GW150914_plotdata.pickle (for further processing by
   [gwcomp_writepaper.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_writepaper.py) )
 * H1_GW150914_stats.txt
 ```
 # snr point_estimate_overlap
 19.826193  0.966952
 ```
 * L1_GW150914_stats.txt
 ```
 # snr point_estimate_overlap
 14.680569  0.971663
 ```
 * NET_GW150914_stats.txt
 ```
 # network_snr point_estimate_net_overlap
 24.669760  0.968121
 ```
SNRs are computed from the median BayesWave waveform.  Overlaps are between the
median BayesWave waveform and the maximum a posteriori waveform from
LALInference.


## Plotting & Printing the Results
 * [gw-comp/gw-comp/gwcomp_writepaper.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_writepaper.py)

Produces time-series plots showing data, burst and LALInference reconstructions.
Also produces a dump to stdout summarising SNRs and overlaps.  See example
below.

```
usage: gwcomp_writepaper.py [-h] --pickled-bayeswave-results BW_PICKLE
                            [--pickled-cwb-results CWB_PICKLE]
                            [--output-filename OUTPUT_FILENAME]
                            [--whiten-scale WHITENORM] [--wavelet-only]
                            [--xlims XLIMS] [--xlims-inset XLIMS_INSET]
                            [--ylims YLIMS] [--ylims-inset YLIMS_INSET]
                            [--reconstruction-residuals]

Produce publication-quality comparison plots of reconstructed time-domain
waveforms from LALInference, BayesWave and cWB. Also prints summary statistics
to stdout. Requires pickled data from gwcomp_driver.py

optional arguments:
  -h, --help            show this help message and exit
  --pickled-bayeswave-results BW_PICKLE
                        Pickled reconstruction data from gwcomp_process.py,
                        using BayesWave median for point estimates
  --pickled-cwb-results CWB_PICKLE
                        Pickled reconstruction data from gwcomp_process.py,
                        using CWB maxL for point estimates
  --output-filename OUTPUT_FILENAME
                        Identifier to attach to output files
  --whiten-scale WHITENORM
                        Normalisation to preserve strain amplitude @ 200 Hz
                        after whitening
  --wavelet-only        boolean to only plot wavelet result
  --xlims XLIMS         x-axis limits for main panel
  --xlims-inset XLIMS_INSET
                        x-axis limits for inset panel
  --ylims YLIMS         y-axis limits for main panel
  --ylims-inset YLIMS_INSET
                        y-axis limits for inset panel
  --reconstruction-residuals
                        boolean to produce insets showing the residuals
                        between reconstruction methods
```


### Example: GW150914
Following from previous examples, we assume the pickle file produced in the
previous step lives in the GW150914 directory:
```
gwcomp_writepaper.py
    --pickled-bayeswave-results GW150914/GW150914_plotdata.pickle \
    --output-filename GW150914 \
    --xlims "0.25 0.45" --xlims-inset "0.35 0.44" \
    --ylims "-18 10" --ylims-inset "-2 2" 
```

Produces the following stdout:
```
********************** RESULTS SUMMARY **********************

--- Point Estimates ---

LI_MAP  = Maximum-likelihood LALInference waveform
CWB_ML  = Maximum-likelihood CWB waveform
BW_MED  = Median BayesWave waveform

* BayesWave SNR: (BW_MED | BW_MED)
    H1:19.83 L1:14.68
    Network:24.67

* CWB SNR: (CWB_ML | CWB_ML)
    H1:nan L1:nan
    Network:nan

* LI SNR: (LI_MAP | LI_MAP)
    H1:20.38 L1:14.17
    Network:24.82

* CWB-LALInference Overlaps: (LI_MAP | CWB_ML) 
    H1: nan L1: nan
    Network: nan

* BayesWave-LALInference Overlaps (LI_MAP | BW_MED) 
    H1: 0.97 L1: 0.97
    Network: 0.97

--- Results From Waveform Posterior Draws ---

* Overlap Posterior (LI | BW) Median +/- 90%-interval
    H1: 0.91 0.93 0.96
    L1: 0.90 0.93 0.96
    Network: 0.91 0.93 0.96
```
We did not supply CWB here so the code just prints nan.

The final plot:
<img src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/H1L1-GW150914-reconstructions.png">


# Residuals

## Making Residuals Frames
 * [gwcomp_residuals.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_residuals.py)
 * Designed to work with NDS2; should be able to make residuals on your laptop

```
usage: gwcomp_residuals.py [-h] [--bw-dir BW_DIR] [--li-samples LI_SAMPLES]
                           [--frame-cache-path FRAME_CACHE_PATH]
                           [--srate SRATE] [--trigtime TRIGTIME]
                           [--approx APPROX] [--duration DURATION]
                           [--frame-type FRAME_TYPE]
                           [--input-channel INPUT_CHANNEL]
                           [--output-channel OUTPUT_CHANNEL] [--make-plots]
                           [--output-path OUTPUT_PATH] [--passband PASSBAND]

Subtract CBC MAP waveform from observational data to get residuals. Waveform
subtraction is performed using whitened data (observational data is whitened
using BayesWave PSDs). The data is de-whitened prior to producing frames with
the residuals.

optional arguments:
  -h, --help            show this help message and exit
  --bw-dir BW_DIR
  --li-samples LI_SAMPLES
  --frame-cache-path FRAME_CACHE_PATH
  --srate SRATE
  --trigtime TRIGTIME
  --approx APPROX
  --duration DURATION
  --frame-type FRAME_TYPE
  --input-channel INPUT_CHANNEL
  --output-channel OUTPUT_CHANNEL
  --make-plots
  --output-path OUTPUT_PATH
  --passband PASSBAND
```

### Example: GW150914
Again, assume we have the results of a LALInference run with an ascii posterior
samples file from LALInference and BayesWave PSDs in an output directory
GW150914:
 * ```GW150914/onsource/posterior_samples.dat```
 * ```GW150914/onsource/IFO0,1_psd.dat```

Then to retrieve 8 seconds of data with the MAP waveform in posterior samples
subtracted *from the passband*
```
gw-comp/gw-comp/gwcomp_residuals.py 
    --bw-dir GW150914/onsource 
    --li-samples GW150914/onsource/posterior_samples.dat 
    --input-channel DCS-CALIB_STRAIN_C02 
    --srate 16384 --trigtime 1126259462.42
    --output-path GW150914/residuals/residuals_frames 
    --passband 32 512
```

Produces
 * H-H1_HOFT_C02-1126259458-8.gwf
 * L-L1_HOFT_C02-1126259458-8.gwf

Which have:

```
[jclark@arawn] $ FrChannels H-H1_HOFT_C02-1126259458-8.gwf
H1:residual 16384
[jclark@arawn] $ FrChannels L-L1_HOFT_C02-1126259458-8.gwf
L1:residual 16384
```


### Example 2: Visualizing  GW150914 Residuals


#### Q-scans
 * Use [gwcomp_qscans.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_qscans.py) to read frames from cache files
 * Assume residual data lives in ```GW150914/residuals```

```
ls GW150914/residuals/residuals_frames/H-H1*gwf | lalapps_path2cache > GW150914/residuals/QSCANS/H1residuals.cache
ls GW150914/residuals/residuals_frames/L-L1*gwf | lalapps_path2cache > GW150914/residuals/QSCANS/L1residuals.cache

echo ...Making Q-scans of residual...
gwcomp_qscans.py --channel residual  --trigtime 1126259462.42
    --datalen 4 --plotwin 0.5 --searchwin 0.5
    --f-min 30 --f-max 600 
    --H1-cache GW150914/residuals/QSCANS/H1residuals.cache \
    --L1-cache GW150914/residuals/QSCANS/L1residuals.cache  \
    --snr-max 2 --xlims 0.25 0.45
```

<a href="https://git.ligo.org/james-clark/gw-comp/blob/master/examples/GW150914-HL-residual-qscan.pdf"><img src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/GW150914-HL-residual-qscan.png"></a>


#### Reconstructed Time Series
 * [gw-comp/gw-comp/gwcomp_driver.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_driver.py):
```
gwcomp_driver.py --bw-dir GW150914/residuals/onsource_residuals/1126259462.42 \
    --li-waveforms GW150914/onsource/waveforms.npy \
    --bw-epoch 1126259460.42 \
    --li-summary-waveforms GW150914/onsource/H1_summary_waveforms.txt \
    GW150914/onsource/L1_summary_waveforms.txt
    --output-filename GW150914-residual --save-for-publication 
    --trigtime 1126259462.42 --li-epoch 1126259460.42 --bw-epoch 1126259460.42 
    --flow 32 --srate 1024
```

 * [gw-comp/gw-comp/gwcomp_writepaper.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_writepaper.py):
```
gwcomp_writepaper.py 
    --pickled-bayeswave-results GW150914/residuals/onsource_residuals/1126259462.42/GW150914_residual_plotdata.pickle  \
    --xlims 0.25 0.45
```

<a href="https://git.ligo.org/james-clark/gw-comp/blob/master/examples/H1L1-GW150914-residual-reconstructions.pdf"><img src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/H1L1-GW150914-residual-reconstructions.png"></a>


## BayesWave Analysis: Computing Fitting Factors / Testing GR
 * [gwcomp_testgr.py](https://git.ligo.org/james-clark/gw-comp/blob/master/gw-comp/gwcomp_testgr.py)

```
usage: gwcomp_testgr.py [-h] --onsource-path ONSOURCE_PATH
                        [--offsource-path OFFSOURCE_PATH] --output-path
                        OUTPUT_PATH [--make-plots] [--save-for-publication]
                        [--map-snr MAP_SNR]

Summarize the residuals analysis. Loads in the Bayes factors and network snr
samples for the residuals analysis. The SNR distribution can be transformed to
a fitting-factor distribution to form the same GR test as was performed for
GW150914. This code also supports background analysis. We compute CDFs for the
off-source Bayes factors and 95th percentile SNRs and compare the on-source
results to get a p-value.

optional arguments:
  -h, --help            show this help message and exit
  --onsource-path ONSOURCE_PATH
                        Path to BayesWave residuals files (e.g., evidence.dat,
                        signal_whitened_moments)
  --offsource-path OFFSOURCE_PATH
                        Path to output directories labelled with GPS times
                        which contain the BayesWave results for off-source
                        trials. Used for p-values
  --output-path OUTPUT_PATH
                        Where to save figures and summary info
  --make-plots
  --save-for-publication
  --map-snr MAP_SNR     The network SNR of the BBH MAP waveform used to
                        construct the residuals
```

### Example: GW150914
Example assumes we have run BayesWave on the residuals constructed in the
preceeding step.

Produces
   * A plot of residual SNR distributions
   * A text dump with summary statistics for SNR and fitting factor


From
[GW150914-TGR-summary.txt](https://git.ligo.org/james-clark/gw-comp/blob/master/examples/GW150914-TGR-summary.txt):

```
onsource netSNR: 0.91 3.53 6.65
CBC MAP netSNR: 24.96
Fitting factor intervals: 0.96 0.99 1.00
GR accuracy > 4%
```

Produces:
<a
href="https://git.ligo.org/james-clark/gw-comp/blob/master/examples/GW150914-resdiual-snr_FF-distributions.pdf"><img
src="https://git.ligo.org/james-clark/gw-comp/raw/master/examples/GW150914-resdiual-snr_FF-distributions.png"></a>

